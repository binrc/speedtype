build: 
	cc ./main.c -lncurses -o speedtype

run: build
	./speedtype

cdump: 
	gdb ./speedtype -c speedtype.core

debug: build
	gcc -ggdb -Wall -Wextra ./main.c -lncurses -o speedtype && gdb ./speedtype

man: 
	mandoc speedtype.1 | less
