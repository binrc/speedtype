#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
//#include <term.h>
//#include <unistd.h>
#include <string.h>
#include <time.h>

size_t size = 0;

int rng(){
	FILE *fp;
	char seed;
	int i = 0;
	fp = fopen("/dev/urandom", "r");
	fread(&seed, 1, 1, fp);
	fclose(fp);
	srand(seed);
	i = rand();
	return i;
}

char *getbuf(int nwords, char *wordlist){
	size = 0;
	printf("Using wordlist: '%s'\n", wordlist);
	unsigned int r = 0;
	int c = 0;
	char casc;
	int i = 0;
	unsigned int lc = 0;
	unsigned int nnl = 0;
	char *buf = malloc(0);

	FILE *fp = fopen(wordlist, "r");

	if(fp == NULL){
		printf("Exiting: No such file or directory\n");
		exit(1);
	}

	// get number of lines
	while((c = fgetc(fp)) != EOF){
		if(c == '\n'){
			lc++;
		}
	}

	// reset
	fseek(fp, 0, 0);

	for(i = 0; i<nwords; i++){
		r = rng() % (lc - 0) + 0;

		// "seek" to specific line
		while((c = fgetc(fp)) != EOF && nnl != r){
			if(c == '\n'){
				nnl++;
			}
		}

		// get first char
		buf = realloc(buf, sizeof(char)*(size+1));
		size++;
		casc = c + '\0';
		buf[size-1] = casc;

		// collect remaining chars into buf
		while((c = fgetc(fp)) != EOF && c != '\n'){
			buf = realloc(buf, sizeof(char)*(size+1));
			size++;
			casc = c + '\0';
			buf[size-1] = casc;
		}

		// add spacing
		buf = realloc(buf, sizeof(char)*(size+1));
		size++;
		casc = ' ' + '\0';
		buf[size-1] = casc;

		// reset seek head
		fseek(fp, 0, 0);
		nnl = 0;
	}
	fclose(fp);
	return buf;
}

char *fortune(char *fcmd){
	size = 0;
	printf("Using command: '%s'\n", fcmd);
	int c = 0;
	char casc;
	int hit = 0;
	char *buf = malloc(0);
	FILE *cmdo = popen(fcmd, "r");	// read from pipe

	while((c = fgetc(cmdo)) != EOF){
		hit = 0;
		buf = realloc(buf, sizeof(char)*(size+1));
		size++;	// need to keep track of pointer size, neither strlen nor sizeof can return this for me 

		while(c == '\n' || c == '\r' || c == '\t' || c == ' '){
			hit = 1;
			c = fgetc(cmdo);
		}

		// add a single space if any whitespace is detected
		if(hit){
			casc = ' ' + '\0';
			buf[size-1] = casc;
			// need to add more bytes for the non-whitespace char
			buf = realloc(buf, sizeof(char)*(size+1));
			size++;
		}

		// add nonwhitespace char
		casc = c + '\0';
		buf[size-1] = casc;
	}

	fclose(cmdo);
	buf[size-1] = '\0'; // remove trailing newline
	return buf;
}


void help(){
	printf("USAGE:\n-w\twordlist\n-n\tnumber of words to load\n-c\tcommand, wrapped in quotes, to use as input\n-l\ttoggle light theme\n");
	exit(0);
}

int results(char *buf, time_t start, time_t stop, unsigned int correct, unsigned int incorrect){
	size_t i = 0;
	time_t total = stop - start;
	float mins = (float)total / 60;
	float accuracy = (float)correct/((float)incorrect + (float)correct) * 100;
	int words = 0; 
	int chars = 0; 

	for(i = 0; i<size; i++){
		if(buf[i] == ' ' || buf[i] == '\t' || buf[i] == '\n')
			words++;
	}

	for(i = 0; i<size; i++){
		chars++;
	}

	float wpm = (float)words / mins;
	float cpm = (float)chars / mins;

	printf("time: %ld seconds\nwords: %d\nchars: %d\naccuracy: %.2f\%\nwpm: %.0f\ncpm: %.0f\n", total, words, chars, accuracy, wpm, cpm);
	return 0;
}



int main(int argc, char *argv[]){
	//scrollok(stdscr, TRUE);
	int i = 1;
	int j = 0;
	int f = 0;
	int light = 0;
	char *defw = "/usr/share/dict/words";
	char *wordlist = malloc(0);
	int count = 20;
	char *buf;

	// input handler
	if(argc > 1){
		for(i; i<argc; i++){
			if(!strcmp(argv[i], "-w")){
				for(j; j<strlen(argv[i+1]); j++){
					wordlist = realloc(wordlist, sizeof(char)*(size+1));
					size++;
					wordlist[size-1] = argv[i+1][j];
				}
			} else if(!strcmp(argv[i], "-n")){
				count = atoi(argv[i+1]);
			} else if(!strcmp(argv[i], "-c")){
				buf = fortune(argv[i+1]);
				f = 1;
			} else if(!strcmp(argv[i], "-h")){
				endwin();
				help();
			} else if(!strcmp(argv[i], "-l")){
				light = 1;
			}

		}
	}

	if(!f){
		if(size == 0){
			buf = getbuf(count, defw);
		} else {
			buf = getbuf(count, wordlist);
		}
	}

	// user input specifics
	int tmp = 0;
	int nch = 0;
	int x = 0;
	int y = 0;
	int maxx = 0;
	int maxy = 0;
	unsigned int correct = 0;
	unsigned int incorrect = 0;

	// curses init
	initscr();
	noecho();
	cbreak();

	// term dimensions
	getmaxyx(stdscr, maxy, maxx);

	// timing
	time_t start = 0;
	time_t stop = 0;
	int started = 0;

	// colors
	start_color();
	init_pair(1, COLOR_BLACK, COLOR_GREEN);	// correct
	init_pair(2, COLOR_BLACK, COLOR_RED);	// incorrect
	init_pair(3, COLOR_WHITE, COLOR_BLACK);	// default
	// light/dark theme
	if(light){
		init_pair(1, COLOR_BLACK, COLOR_GREEN);	// correct
		init_pair(2, COLOR_WHITE, COLOR_RED);	// incorrect
		init_pair(3, COLOR_BLACK, 15);	// default
		wbkgd(stdscr, COLOR_PAIR(3));
	}

	// text to type
	for(i = 0; i < size; i++)
		printw("%c", buf[i]);


	// reset cursor
	move(0,0);
	refresh();

	/* get ready for spaghetti */

	// loop until eob 
	while(nch < size){
		tmp = getch();
		if(!started){
			started = 1;
			start = time(NULL);
		}

		getyx(stdscr, y, x);

		// catch all types of \b
		if(nch != 0){
			switch (tmp) {
				case KEY_BACKSPACE:
				case 127:
				case '\b':
					if(y > 0 && x == 0){
						nch--;
						y = y -1;
						x = maxx - 1; // needs to be max width -1 because starts at 0
						move(y, x);
						printw("%c", buf[nch]);
						move(y, x);
					} else {
						nch--;
						getyx(stdscr, y, x);
						move(y, x-1);
						printw("%c", buf[nch]);
						move(y, x-1);
					}
					goto nexti;
					break;
			}
		}
		if(tmp == buf[nch]){
			attron(COLOR_PAIR(1));
			printw("%c", buf[nch]);
			attroff(COLOR_PAIR(1));
			correct++;
		} else if (tmp != buf[nch]){
			attron(COLOR_PAIR(2));
			printw("%c", buf[nch]);
			attroff(COLOR_PAIR(2));
			incorrect++;
		}

		nch++;
nexti:
		refresh();

	}

	stop = time(NULL);
	refresh();
	endwin();
	results(buf, start, stop, correct, incorrect);
	free(buf);
	free(wordlist);

	return 0;
}

