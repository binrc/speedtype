# BUILGING:

Requires ncurses. 

```shell
$ make 
```

Manpage, requires mandoc. 

```shell
$ make man
```

# USAGE:

```shell
-w	wordlist
-n	number of words to load
-c	command, wrapped in quotes, to use as input
-l	toggle light theme
```

# DESCRIPTION: 

A curses based typing speed test.

If no arguments are supplied, test uses `/usr/share/dict/words` for it's wordlist and selects 20 words. The test can be aborted by pressing C-c.

```shell
The following arguments are available:

     -h       Print help info.

     -l       Toggle light theme.

     -w       Set the (newline delimited) wordlist.

     -n       Set number of words to choose from the wordlist.

     -c       Use a shell command as input text.
```

# EXAMPLES:

Use a specific wordlist, number of words, and toggle the light theme: 
```shell
$ speedtype -w /wordlists/top100.txt -n 10 -l
```

Use the output of a command as input text:
```shell
$ speedtype -c "cat sc/hello.c"
```

# EXTRAS:
Some wordlists are includes in `wordlists/`. Samples of source code are included in `sc/`. These are to be used as input text. 
